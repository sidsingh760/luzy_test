import React from 'react';
import {View, TextInput, StatusBar, StyleSheet} from 'react-native';
import Constant from "../reference/dimensions";
import Color from "../reference/colors";
import {height_change} from "../reference/deviceSupport";

const C_text = () => {
    return (
        <View>
            <StatusBar backgroundColor={Color.header} barStyle="light-content" />
            <View style={{...Constant.style.container, ...styles.container }}>
                <TextInput/>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: Color.header,
        paddingTop: height_change('0.2%'),
        paddingBottom: height_change('3.1%')
    }
});

export {C_text}