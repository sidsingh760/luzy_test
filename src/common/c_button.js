import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    TouchableOpacity, View,
} from "react-native";
import Constant from '../reference/dimensions';
import Color from "../reference/colors";
import Font from "../reference/fonts";
import {width_change as wp, height_change as hp} from '../reference/deviceSupport';

const C_button = (props) => {
    const {btnOuter,btnText} = styles;
    const {title, onPress, extraStyle, containerStyle,textStyle} = props;
    return(
        <TouchableOpacity style={[btnOuter,containerStyle && containerStyle]}
                          onPress={onPress}>
            <Text style={[btnText,textStyle&&textStyle]}>
                {title}
            </Text>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    btnOuter:{
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: hp('1.6%'),
        backgroundColor: Color.lightblue,
        borderRadius:5
    },
    btnText:{
        color: Color.white_main,
        fontSize: Font.fontSize.small,
        fontFamily: Font.font.linotte_heavy
    }
});

export {C_button};
