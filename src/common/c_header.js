import React from 'react';
import {View, Image, StatusBar, StyleSheet} from 'react-native';
import Constant from "../reference/dimensions";
import Color from "../reference/colors";
import {height_change, width_change} from "../reference/deviceSupport";

const C_header = () => {
    return (
        <View>
            <StatusBar backgroundColor={Color.header} barStyle="light-content" />
            <View style={{...Constant.style.container, ...styles.container }}>
                <Image source={require('../assets/pictures/luzy_logo.png')}
                       style={{height: height_change('5%'),
                           width: width_change('30%'),marginLeft: -width_change('5%') }}
                       resizeMode={'contain'}/>
                <Image source={require('../assets/pictures/i_menu.png')}
                       style={{height:height_change('5%'), width:30, marginRight: -width_change('2%')}} resizeMode={'contain'}/>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: Color.header,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: height_change('0.5%'),
        paddingBottom: height_change('2.3%')
    }
});

export {C_header}