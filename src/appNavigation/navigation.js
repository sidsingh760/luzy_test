import {
    createStackNavigator,
    createAppContainer,
} from 'react-navigation';
import Login  from "../screens/loginScreen"
import Main from "../ui/main";

const appNavigation = createStackNavigator({
        Login,
        Main
    },
    {headerMode: 'none'},
);

export default createAppContainer(appNavigation)