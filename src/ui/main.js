import React, { Component } from 'react';
import {SafeAreaView, StyleSheet, Text, View} from 'react-native';
import Color from "../reference/colors";
import Font from "../reference/fonts";
import {C_header} from "../common/combine";

export default class Main extends Component {
    render() {
        const {textTitle} = style;
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: Color.blue}}>
                <C_header/>
                <View style={{justifyContent: 'center', flex: 1, backgroundColor: Color.lightblue}}>
                    <Text style={textTitle}>Main Screen</Text>
                </View>
            </SafeAreaView>
        )
    }
}

const style = StyleSheet.create({
    textTitle:{
        fontSize:Font.fontSize.big,
        fontFamily:Font.font.roboto_bold,
        color:Color.black_main,
        textAlign: 'center'
    }
});