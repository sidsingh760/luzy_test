import React, {Component} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    Keyboard,
    View,
    TextInput,
    Image,
    Alert
} from 'react-native';
import {StackActions, NavigationActions} from 'react-navigation'
import Constant from '../reference/dimensions';
import Color from "../reference/colors";
import Font from "../reference/fonts";
import {C_button, C_header} from "../common/combine";
import {height_change} from '../reference/deviceSupport';
import {validateEmail} from "../reference/validation";

export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            emailID: '',
            password: '',
            rememberMe: false,
            showPassword: false,
        }
    }

    onPressRememberMe = () => {
        this.setState({
            rememberMe: !this.state.rememberMe
        });
    };

    onPressShowPassword = () => {
        this.setState({
            showPassword: !this.state.showPassword
        });
    };

    onPressLoginBtn = () => {
        const {emailID, password} = this.state
        Keyboard.dismiss();

        if (!validateEmail(emailID)) {
            alert('Please enter valid email address')
        } else if (password.length === 0) {
            alert('Please enter password')
        } else {
            const {handleLocalAction, localActions} = this.props;
            handleLocalAction({type: localActions.LOGIN, emailID, password})
        }
    };

    onEmailBlur = () => {
        if (this.refs.txtPassword) {
            this.refs.txtPassword.focus();
        }
    };

    componentWillReceiveProps(nextProps) {
        debugger
        if (nextProps && nextProps.userData && nextProps.userData[0] && nextProps.userData[0].Token) {
            this.props.navigation.dispatch(StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({routeName: 'Main'})]
            }));
        } else {
            Alert.alert('Please enter valid credential')
        }
    }

    render() {
        const {
            container,
            textTitle,
            subTitle,
            inputIcon,
            inputRightIcon,
            socialIcon,
            inputLabel,
            textInput,
            inputContainer,
            bottomText,
            textContainer,
            rememberMeContainer
        } = styles;

        const {emailID, password, rememberMe, showPassword} = this.state;

        return (
            <SafeAreaView style={container}>
                <C_header/>
                <View style={textContainer}>
                    <Text style={textTitle}>
                        {'SIGN IN'}
                    </Text>
                    <Text style={subTitle}>
                        {'with LUZY account.'}
                    </Text>
                    <View style={{width: Constant.screenWidth * 0.85, alignSelf: 'center'}}>
                        <Text style={inputLabel}>USERNAME:</Text>
                        <View style={inputContainer}>
                            <Image source={require('../assets/pictures/i_placeholeder_email.png')}
                                   style={inputIcon} resizeMode={'contain'}/>
                            <TextInput placeholder={'Your email address'}
                                       numberOfLines={1}
                                       ref={'txtEmail'}
                                       autoCapitalize="none"
                                       autoCorrect={false}
                                       returnKeyType={'next'}
                                       placeholderTextColor={Color.text_main}
                                       style={textInput}
                                       value={emailID}
                                       onChangeText={(emailID) => this.setState({emailID})}
                                       onSubmitEditing={this.onEmailBlur}
                                       underlineColorAndroid={Color.transparent}
                            />
                            {
                                validateEmail(emailID) &&
                                <Image source={require('../assets/pictures/i_checked.png')}
                                       style={inputRightIcon} resizeMode={'contain'}/>
                            }

                        </View>
                        <Text style={inputLabel}>PASSWORD:</Text>
                        <View style={inputContainer}>
                            <Image source={require('../assets/pictures/i_placeholeder_password.png')}
                                   style={inputIcon} resizeMode={'contain'}/>
                            <TextInput placeholder={'Your password'}
                                       numberOfLines={1}
                                       ref={'txtPassword'}
                                       placeholderTextColor={Color.text_main}
                                       style={textInput}
                                       autoCapitalize="none"
                                       autoCorrect={false}
                                       value={password}
                                       onChangeText={(password) => this.setState({password})}
                                       underlineColorAndroid={Color.transparent}
                                       secureTextEntry={!showPassword}
                            />
                            <TouchableOpacity onPress={this.onPressShowPassword}>
                                <Image
                                    source={showPassword && require('../assets/pictures/i_show_password_on.png') || require('../assets/pictures/i_show_password_off.png')}
                                    style={inputRightIcon} resizeMode={'contain'}/>
                            </TouchableOpacity>
                        </View>
                        <View style={{
                            flexDirection: 'row', marginTop: 3,
                            marginBottom: height_change('1.5%'), alignItems: 'center', justifyContent: 'space-between'
                        }}>
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <TouchableOpacity onPress={this.onPressRememberMe}>
                                    <View style={{
                                        ...inputRightIcon, ...rememberMeContainer,
                                        backgroundColor: rememberMe && Color.lightblue || Color.transparent
                                    }}>
                                        {
                                            rememberMe &&
                                            <Image
                                                source={require('../assets/pictures/i_remember_me_checkmark.png')}
                                                style={{height: '95%', width: '95%'}} resizeMode={'contain'}/>
                                        }

                                    </View>
                                </TouchableOpacity>
                                <Text style={[bottomText, {
                                    color: Color.white_main,
                                    fontFamily: Font.font.roboto_regular
                                }]}>
                                    {'Remember me'}</Text>
                            </View>
                            <Text style={[bottomText, {
                                color: Color.white_main,
                                fontFamily: Font.font.roboto_regular
                            }]}>
                                {'Forgot your password'}</Text>
                        </View>
                        <C_button
                            containerStyle={{backgroundColor: Color.lightblue}}
                            textStyle={{color: Color.white_main}}
                            title={'LOGIN'}
                            onPress={this.onPressLoginBtn}
                        />
                    </View>
                </View>

                <View style={{backgroundColor: Color.blue, alignItems: 'center', paddingVertical: height_change('2.5%')}}>
                    <Text style={textTitle}>
                        {'SIGN IN'}
                    </Text>
                    <Text style={subTitle}>
                        {'with your social media account.'}
                    </Text>
                    <View style={{flexDirection: 'row', alignSelf: 'center', marginTop: height_change('1.5%')}}>
                        <Image source={require('../assets/pictures/i_facebook.png')}
                               style={socialIcon} resizeMode={'contain'}/>
                        <Image source={require('../assets/pictures/i_twitter.png')}
                               style={[socialIcon, {marginHorizontal: 20}]} resizeMode={'contain'}/>
                        <Image source={require('../assets/pictures/i_instagram.png')}
                               style={socialIcon} resizeMode={'contain'}/>
                    </View>
                </View>

                <View style={{
                    backgroundColor: Color.white_main,
                    flex: 1, paddingVertical: height_change('2.5%')
                }}>
                    <View style={{...Constant.style.container, flex: 1}}>
                        <Text style={{...textTitle, color: Color.text_main}}>
                            {'SIGN UP'}
                        </Text>
                        <C_button
                            containerStyle={{backgroundColor: Color.text_main}}
                            textStyle={{color: Color.white_main}}
                            title={'CREATE ACCOUNT'}
                        />
                    </View>
                    <View style={{...Constant.style.container, flexDirection: 'row', justifyContent: 'space-between'}}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Image source={require('../assets/pictures/i_about_us.png')}
                                   style={inputRightIcon} resizeMode={'contain'}/>
                            <Text style={{...bottomText, fontFamily: Font.font.roboto_bold}}>
                                {'About us'}</Text>
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Image source={require('../assets/pictures/i_need_help.png')}
                                   style={inputRightIcon}
                                   resizeMode={'contain'}/>
                            <Text style={{...bottomText, fontFamily: Font.font.roboto_bold}}>
                                {'Need help?'}</Text>
                        </View>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.header
    },
    textTitle: {
        fontSize: Font.fontSize.x_big,
        fontFamily: Font.font.linotte_bold,
        color: Color.white_main,
        textAlign: 'center'
    },
    subTitle: {
        fontSize: Font.fontSize.x_small,
        fontFamily: Font.font.roboto_regular,
        color: Color.white_main,
        textAlign: 'center'
    },
    socialIcon: {
        height: height_change('6%'),
        width: height_change('6%'),
    },
    inputContainer: {
        backgroundColor: Color.textBox,
        padding: height_change('0.5%'),
        flexDirection: 'row',
        marginTop: 3,
        borderRadius: 5,
        marginBottom: 15,
        alignItems: 'center'
    },
    inputRightIcon: {
        height: height_change('3%'),
        width: height_change('3%')
    },
    inputIcon: {
        height: height_change('4%'),
        width: height_change('4%')
    },
    inputLabel: {
        fontSize: Font.fontSize.x_small,
        color: Color.white_main,
        fontFamily: Font.font.linotte_heavy
    },
    bottomText: {
        fontSize: Font.fontSize.x_small,
        marginLeft: 5,
        color: Color.lightGrey,
        fontFamily: Font.font.roboto_bold
    },
    textContainer: {
        backgroundColor: Color.black_main,
        paddingVertical: height_change('2.5%')
    },
    rememberMeContainer: {
        borderRadius: 5,
        borderColor: Color.white_main,
        padding: 2,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2
    },
    textInput: {
        fontSize: Font.fontSize.x_small,
        fontFamily: Font.font.robotoRegular,
        padding: height_change('0.7%'),
        color: Color.white_main, flex: 1
    },
});
