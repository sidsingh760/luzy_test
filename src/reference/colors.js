module.exports = {
    header: '#054993',
    black_main: '#02152a',
    white_main: '#ffffff',
    lightGrey: '#b5c8dd',
    blue: "#054993",
    transparent: 'transparent',
    lightblue: '#0cb6f3',
    text_main: '#054993',
    textBox: '#0a1d41',
    btn: "#00cb6f3",
    pink_main:"FFC0CB",
    fb: "#3667b8",
    twitter: "#00a3f9",
    insta: "#e50069"
};