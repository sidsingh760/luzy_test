import { Dimensions, PixelRatio, Platform } from 'react-native';

let displayHeight = Platform.OS === 'iOS' && Dimensions.get('window').height || Dimensions.get('window').height - 40;
let displayWidth = Dimensions.get('window').width;

const height_change = hpercent => {
    const height = typeof hpercent === "number" ? hpercent : parseFloat(hpercent);
    return PixelRatio.roundToNearestPixel(displayHeight * height / 100);
};

const width_change = wpercent => {
    const width = typeof wpercent === "number" ? wpercent : parseFloat(wpercent);
    return PixelRatio.roundToNearestPixel(displayWidth * width / 100);
};

export {
    height_change,
    width_change
};