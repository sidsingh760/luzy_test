import {Dimensions, PixelRatio, Platform} from "react-native";
const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window');

const s = SCREEN_WIDTH / 375;

export function normalize(size) {
    const size_updated = size * s;
    if (Platform.OS === 'ios') {
        return Math.round(PixelRatio.roundToNearestPixel(size_updated))
    } else {
        return Math.round(PixelRatio.roundToNearestPixel(size_updated)) - 2
    }
}
module.exports = {
    font: {
        roboto_regular: 'Roboto-Regular',
        roboto_bold: 'Roboto-Bold',
        linotte_bold: 'Linotte-Bold',
        linotte_heavy: 'Linotte-Heavy'
    },
    fontSize: {
        xx_small: normalize(10),
        x_small: normalize(12),
        small: normalize(14),
        medium: normalize(18),
        big: normalize(20),
        x_big: normalize(30)
    }
};