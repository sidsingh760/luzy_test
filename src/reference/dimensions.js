import { Platform, Dimensions, PixelRatio } from 'react-native';

const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window');

module.exports = {
    style:{
        container:{
            width: SCREEN_WIDTH * 0.85,
            alignSelf: 'center'
        }
    },
    screenHeight:  Platform.OS === 'ios' && SCREEN_HEIGHT || SCREEN_HEIGHT - 24,
    screenWidth:  SCREEN_WIDTH,
    screen: Dimensions.get('window'),
    fullScreenHeight:  SCREEN_HEIGHT,
    isAndroid: Platform.OS === 'android',
    isiPad: ((SCREEN_HEIGHT/SCREEN_WIDTH) < 1.6),
    isiOS: Platform.OS === "ios"
};