import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import appReducer from '../reducers/indexReducer';
import AppNavigator from '../appNavigation/navigation';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { PersistGate } from 'redux-persist/integration/react'


const persistConfig = {
    key: 'root',
    storage,
};

const persistance_reducer = persistReducer(persistConfig, appReducer);
let persistance_store = createStore(persistance_reducer,applyMiddleware(thunk));
let main_persistor = persistStore(persistance_store);

export default class store extends React.Component {

    render() {
        return (
            <Provider store={persistance_store}>
                <PersistGate loading={null} persistor={main_persistor}>
                    <AppNavigator/>
                </PersistGate>
            </Provider>
        );
    }
}