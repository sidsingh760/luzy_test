import { combineReducers } from 'redux';
import LoginReducer from "./loginReducer";

export default appReducer = combineReducers({
    Login: LoginReducer,
});
